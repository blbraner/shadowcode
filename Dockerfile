FROM python:3.8.0-buster
COPY . /app
WORKDIR /app
RUN pip install pipenv && pipenv run pip install pip==18.0 &&  pipenv install --system
EXPOSE 80 443
#CMD /bin/bash
CMD bash -c "pipenv install --system && flask run -h 0.0.0.0 -p 80"