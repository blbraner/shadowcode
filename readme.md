# Variable Change 

Variable Change is a SaaS product that allows you to manage your feature flags for releasing partially done features along
with your other production ready code. You can turn features on or off from a separate web interface.

Why would you want to use a feature flag to release code that is not ready for production? There are several reasons
for this, starting with the agile principal of code always being in  a state of releasability. You should be able to 
ship your code at anytime without effecting the usability of your product.

Feature flagging your work will also keep your branches from becoming stale as you will be able to merge your code into 
develop without the fear of breaking someone else features or releasing unfinished work.

## [Project Board](https://app.clubhouse.io/timberln/project/11/shadowcode)

## [Git Repo](https://gitlab.com/blbraner/shadowcode)

## [API Documentation](https://documenter.getpostman.com/view/91633/SW131xtA?version=latest)

##Running Locally
 
Variable Change is built on Python's Flask web framework running on Docker.
To run locally you will need to have Docker and Docker Compose installed on your machine.
* Windows and Mac https://www.docker.com/products/docker-desktop
* Linux https://docs.docker.com/install/ and choose your distribution 


##Development Standards
* PEP-8
* Branch naming (bugfix,feature,epic)/shortdescription-clubhouse-ticket-number  example `feature/setup-flask-10`

## Troubleshooting Setup


