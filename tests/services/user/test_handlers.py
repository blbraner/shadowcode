from app.services.user.handlers import User
from faker import Faker
import json
from app.models.user import User
from tests.model_fixtures import shadow_admin_user


class TestUser(object):

    def teardown_method(self, method):
        User.drop_collection()

    def test_create_user(self, test_client, shadow_admin_user):
        fake = Faker()
        firstname = fake.first_name()
        lastname = fake.last_name()
        username = fake.ascii_email()
        password = fake.password()

        body = {
            "username": username,
            "password": password,
            "first_name": firstname,
            "last_name": lastname
        }

        authorization_token = f"Bearer {shadow_admin_user.api_token}"
        response = test_client.post(
            '/api/v1/register',
            data=json.dumps(body),
            headers={"content-type": "application/json", 'Authorization': authorization_token}
        )

        response_object = response.get_json()
        assert response is not None
        assert len(response_object.get('api_token')) > 0

    def test_create_user_missing_username(self, test_client, shadow_admin_user):
        fake = Faker()
        firstname = fake.first_name()
        lastname = fake.last_name()
        password = fake.password()

        body = {
            "password": password,
            "first_name": firstname,
            "last_name": lastname
        }

        authorization_token = f"Bearer {shadow_admin_user.api_token}"
        response = test_client.post(
            '/api/v1/register',
            data=json.dumps(body),
            headers={"content-type": "application/json", 'Authorization': authorization_token}
        )

        assert response.status_code == 400

    def test_create_user_missing_password(self, test_client, shadow_admin_user):
        fake = Faker()
        firstname = fake.first_name()
        lastname = fake.last_name()
        username = fake.ascii_email()

        body = {
            "username": username,
            "first_name": firstname,
            "last_name": lastname
        }
        authorization_token = f"Bearer {shadow_admin_user.api_token}"
        response = test_client.post(
            '/api/v1/register',
            data=json.dumps(body),
            headers={"content-type": "application/json", 'Authorization': authorization_token}
        )

        assert response.status_code == 400
