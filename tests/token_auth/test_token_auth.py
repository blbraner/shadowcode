from app.decorators.token_auth import api_auth_required, api_auth_required_super_admin
from app.models.user import User
from config import SUPER_ADMIN_ROLE
from tests.model_fixtures import user_dict, super_admin_user_dict
from unittest import mock
from werkzeug.exceptions import Unauthorized
import pytest


class TestTestTokenAuth(object):
    @api_auth_required
    def api_route_method(self, user):
        return user

    def test_api_auth_required(self, app, user_dict):
        with app.test_request_context(headers={'Authorization': "Bearer Test"}):
            with mock.patch.object(User, 'objects', return_value=[user_dict]):
                func = lambda x: x

                decorator = api_auth_required(func)
                user = decorator()
                assert user['active']

    def test_api_auth_required_no_token(self, app, user_dict):
        del (user_dict['api_token'])
        with app.test_request_context(headers={'Authorization': "Bearer Test"}):
            func = lambda x: x
            with pytest.raises(Unauthorized) as excinfo:
                decorator = api_auth_required(func)
                user = decorator()

    def test_api_auth_required_super_admin_no_super_admin_role(self, app, user_dict):
        with app.test_request_context(headers={'Authorization': "Bearer Test"}):
            with mock.patch.object(User, 'objects', return_value=[user_dict]):
                func = lambda x: x

                with pytest.raises(Unauthorized) as excinfo:
                    decorator = api_auth_required_super_admin(func)
                    user = decorator()

    def test_api_auth_required_super_admin_with_super_admin_role(self, app, super_admin_user_dict):
        with app.test_request_context(headers={'Authorization': "Bearer Test"}):
            with mock.patch.object(User, 'objects', return_value=[super_admin_user_dict]):
                func = lambda x: x

                decorator = api_auth_required(func)
                user = decorator()
                assert user['active']