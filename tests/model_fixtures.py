import json
import pytest
from bson import ObjectId

from app.models.user import User
from config import SUPER_ADMIN_ROLE


@pytest.fixture()
def shadow_admin_user():
    shadow_admin_user = User()
    shadow_admin_user.first_name = 'admin'
    shadow_admin_user.last_name = 'user'
    shadow_admin_user.username = 'admin@shadowcode.com'
    shadow_admin_user.password = 'password'
    shadow_admin_user.roles = ['shadow_admin']
    shadow_admin_user.api_token = shadow_admin_user.create_api_token()
    shadow_admin_user.save()
    return shadow_admin_user


@pytest.fixture()
def super_admin_user_dict():
    user_dict = {"_id" : ObjectId("5db71fb2901c23bc921b7882"),
        "active" : True,
        "username" : "Test",
        "password" : "pbkdf2:sha256:150000$9AqW4AD6$a6e7a689026a0d54db92b72ff4dbd6e23faa597279f14a4dd03c91414982af3f",
        "first_name" : "Johnny",
        "last_name" : "Test",
        "api_token" : "WSqKqUizUNPABLxmRV_P-UJ42tQ",
        "roles" : [
           SUPER_ADMIN_ROLE
        ]
    }
    return user_dict

@pytest.fixture()
def user_dict():
    user_dict = {"_id" : ObjectId("5db71fb2901c23bc921b7882"),
        "active" : True,
        "username" : "Test",
        "password" : "pbkdf2:sha256:150000$9AqW4AD6$a6e7a689026a0d54db92b72ff4dbd6e23faa597279f14a4dd03c91414982af3f",
        "first_name" : "Johnny",
        "last_name" : "Test",
        "api_token" : "WSqKqUizUNPABLxmRV_P-UJ42tQ",
        "roles" : []
    }
    return user_dict
