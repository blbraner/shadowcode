from flask import Response


class ResponseJson(Response):
    default_mimetype = 'application/json'
