import datetime
from flask import jsonify

from app.models.feature_flags import FeatureFlag as Model
from app.models.feature_flags import FeatureFlagEnvironment


class FeatureFlag(object):

    @staticmethod
    def create_feature_flag(name, user_id, type, data):
        feature_flag = Model()
        feature_flag.name = name
        feature_flag.user_id = user_id
        feature_flag.type = type
        development = FeatureFlagEnvironment()
        development.active = data['environments']['development']['active'] or False

        staging = FeatureFlagEnvironment()
        staging.active = data['environments']['staging']['active'] or False

        production = FeatureFlagEnvironment()
        production.active = data['environments']['production']['active'] or False

        feature_flag.environments['development'] = development
        feature_flag.environments['staging'] = staging
        feature_flag.environments['production'] = production

        feature_flag.created_date = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

        feature_flag.save()
        return feature_flag

    @staticmethod
    def update_feature_flag(name, user_id, data):

        feature_flag = Model.objects(id=data['id'], user_id=user_id).first()

        if not feature_flag:
            raise ValueError('Feature Flag could not be found')

        if name:
            feature_flag.name = name

        if data['environments']['development']:
            feature_flag['environments']['development']['active'] = data['environments']['development']['active']

        if data['environments']['staging']:
            feature_flag['environments']['staging']['active'] = data['environments']['staging']['active']

        if data['environments']['production']:
            feature_flag['environments']['production']['active'] = data['environments']['production']['active']

        feature_flag.save()
        return feature_flag
