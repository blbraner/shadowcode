import datetime
import json
from pymongo.errors import DuplicateKeyError

from mongoengine import ValidationError

from app.services.feature_flag import bp
from app.helpers.responses import ResponseJson
from flask import request, escape, jsonify, abort, Response
from app.decorators.token_auth import api_auth_required
from app.services.feature_flag.handlers import FeatureFlag
from app.models.feature_flags import FeatureFlag as FeatureFlagModel
from mongoengine.errors import NotUniqueError

@bp.route('/api/v1/featureflag', methods=['POST'])
@api_auth_required
def create_feature_flag(user):
    status_code = 201
    data = request.get_json()
    name = escape(data.get('name', None))
    user_id = user.id
    type = data.get('type', None)
    if name is None:
        abort(400)
    if type is None:
        abort(400)

    try:
        new_flag = FeatureFlag.create_feature_flag(name, user_id, type, data)
        response = {'id': str(new_flag['id']), 'message': 'success'}
    except ValueError as err:
        message = "Could not create feature flag"
        response = {"message": message}
        status_code = 400
    except NotUniqueError:
        message = f"Feature Flag with {name} already exists for this user"
        response = message
        status_code = 400
    return jsonify(response), status_code


@bp.route('/api/v1/featureflag', methods=['PATCH'])
@api_auth_required
def update_feature_flag(user):
    status_code = 200
    data = request.get_json()
    if not data['id']:
        abort(400)

    if type is None:
        abort(400)

    name = escape(data.get('name', None))
    user_id = user.id

    # TODO add logging for exceptions
    try:
        updated_flag = FeatureFlag.update_feature_flag(name, user_id, data)
        response = {'id': str(updated_flag['id']), 'message': 'success'}
    except ValidationError:
        message = "Invalid id"
        response = {"message": message}
        status_code = 400
    except ValueError as err:
        message = f"Could not locate feature flag {data['id']} for user {user_id}"
        response = {"message": message}
        status_code = 400
    except NotUniqueError:
        message = f"Feature Flag with {name} already exists for this user"
        response = message
        status_code = 400
    return jsonify(response), status_code


@bp.route('/api/v1/featureflag', methods=['GET'])
@api_auth_required
def get_feature_flags(user):
    user_id = user.id
    feature_flags = FeatureFlagModel.objects(user_id=user_id).exclude('user_id')
    if len(feature_flags) == 0:
        response = {"message": 'No feature flags found'}
        return ResponseJson(jsonify(response), status=400)

    response = list()
    for i in range(len(feature_flags)):
        ff = dict()
        ff['id'] = str(feature_flags[i].id)
        ff['name'] = feature_flags[i]['name']
        ff['type'] = feature_flags[i]['type']
        ff['environments'] = feature_flags[i]['environments']
        response.append(ff)

    if len(response) == 0:
        response = {"message": f"No feature flags for user {user.id} found"}

    return jsonify(response), 200
