from flask import Blueprint

bp = Blueprint('api_feature_flags', __name__)

from app.services.feature_flag import api_routes
