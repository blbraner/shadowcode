from flask import Blueprint

bp = Blueprint('api_user', __name__)

from app.services.user import api_routes
