from app.services.user import bp
from app.helpers.responses import ResponseJson
from flask import request, jsonify
from app.services.user.handlers import User
from app.decorators.token_auth import api_auth_required_super_admin


@bp.route('/api/v1/register', methods=['POST'])
@api_auth_required_super_admin
def register(user):
    data = request.get_json()
    username = data.get('username', None)
    password = data.get('password', None)
    if username is None:
        response = {'message': 'please set username'}
        return jsonify(response), 400

    if password is None:
        response = {'message': 'please set password'}
        return jsonify(response), 400

    first_name = data.get('first_name', None)
    last_name = data.get('last_name', None)

    user = User.create_user(username, password, first_name, last_name)
    if user is None:
        response = {"message": "User could not be created"}
        return ResponseJson(jsonify(response))
    return jsonify({'message': 'success', 'api_token': user.api_token})
