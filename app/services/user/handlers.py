from app.models.user import User as Model
from werkzeug.security import generate_password_hash
from flask import escape

class User(object):

    @staticmethod
    def create_user(username, password, first_name, last_name):
        try:
            user = Model()
            user.username = escape(username)
            user.password = generate_password_hash(password)
            user.first_name = escape(first_name)
            user.last_name = escape(last_name)
            user.api_token = user.create_api_token()
            user.save()
            return user
        except:
            return None
