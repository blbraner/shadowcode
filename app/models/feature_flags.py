from mongoengine import *


class FeatureFlagEnvironment(EmbeddedDocument):
    active = BooleanField(default=False)


class FeatureFlag(Document):
    name = StringField(unique_with='user_id', required=True)
    user_id = ReferenceField('User', required=True)
    type = StringField(required=True)
    environments = MapField(EmbeddedDocumentField(FeatureFlagEnvironment))
    created_date = DateField()
