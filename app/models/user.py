import secrets
from flask_user import UserMixin
from mongoengine import *
from app.models.feature_flags import FeatureFlag


class User(Document, UserMixin):
    active = BooleanField(default=True)

    # User authentication information
    username = StringField(required=True)
    password = StringField(required=True)

    # User information
    first_name = StringField(default='')
    last_name = StringField(default='')

    api_token = StringField()
    # Relationships
    roles = ListField(StringField(), default=[])

    def create_api_token(self):
        return secrets.token_urlsafe(20)
