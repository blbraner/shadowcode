from app.models.user import User
from flask import request, jsonify, abort
from functools import wraps
from config import SUPER_ADMIN_ROLE


def api_auth_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user = get_user_from_bearer(*args, **kwargs)
        return f(user, *args, **kwargs)

    return decorated_function


def api_auth_required_super_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user = get_user_from_bearer(*args, **kwargs)

        if SUPER_ADMIN_ROLE not in user['roles']:
            abort(401)
        return f(user, *args, **kwargs)

    return decorated_function


def get_user_from_bearer(*args, **kwargs):
    if not 'Authorization' in request.headers:
        abort(401, 'Missing Authorization Header')
    data = request.headers['Authorization']
    token = str.replace(str(data), 'Bearer ', '')
    user = User.objects(api_token=token).first()

    if len(user) == 0:
        abort(401, 'Could not find user')

    if not user['active']:
        abort(401, 'User not active')
        
    return user
