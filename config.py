import os

SUPER_ADMIN_ROLE = "super_admin"


class Config(object):

    MONGODB_SETTINGS = {
        'db': 'variablechange',
        'host': 'mongo',
        'port': 27017,
        'username': 'root',
        'password': 'password',
        'authentication_source': 'admin'
    }

    SECRET_KEY = os.getenv('SECRET_KEY', '2D297859583F4C9E8BD8534E718E1')

    # flask_user setup
    USER_APP_NAME = "Variable Change"  # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = False  # Disable email authentication
    USER_ENABLE_USERNAME = True  # Enable username authentication
    USER_REQUIRE_RETYPE_PASSWORD = False  # Simplify register form
    USER_LOGIN_URL = '/login'
    USER_LOGOUT_URL = '/logout'
    USER_REGISTER_URL = '/signup'


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    MONGODB_SETTINGS = {
        'db': 'variablechange_test',
        'host': '127.0.0.1',
        'port': 27017,
        'username': 'root',
        'password': 'password',
        'authentication_source': 'admin'
    }


class ProductionConfig(Config):
    TESTING = False
    DEBUG = False
    MONGODB_SETTINGS = {
        'host': 'mongodb+srv://brandon:QMDOtNGZZ3bnmdLe@freecluser-zkdoa.mongodb.net/test?retryWrites=true&w=majority',
    }